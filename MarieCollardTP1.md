# Tp1
## I. Self-footprinting
## Host OS

Pour trouver le nom de la machine je fais "**[system.environment]::MachineName**" dans mon Powershell.

- *Ce qui m'affiche* :

**LAPTOP-B775J4NC**
--- ---

Pour connaître l'OS et sa versionn je fais "**(Get-WmiObject -class Win32_OperatingSystem).Caption**".

- *Ce qui m'affiche* : 

**Microsoft Windows 10 Famille.**
--- -- -

Pour connaître l'architecture du processeur je fais "**Get-CimInstance -ClassName Win32_Processor** | **Select-Object** -**ExcludeProperty** **"CIM*"**"".

- *Ce qui m'affiche* : 

DeviceID Name                                     Caption                                MaxClockSpeed SocketDesignation Manufacturer

CPU0     Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz **Intel64** Family 6 Model 142 Stepping 11 1992          U3E1              GenuineIntel
- L'architecture de mon processeur est de 64.
--- ----

Pour connaître le modèle de ma RAM "**Get-CimInstance win32_physicalmemory | Format-Table Manufacturer, PartNumber**".

- *Ce qui m'affiche* : 

Manufacturer PartNumber

SK Hynix     HMA851S6AFR6N-UH
SK Hynix     **HMA851S6AFR6N-UH**
- Le modèle de ma RAM est donc HMA851S6AFR6N-UH.
---- ----


Pour connaître la capacité de ma RAM je fais "**Get-CimInstance win32_physicalmemory | Format-Table Capacity**".

- *Ce qui m'affiche* : 

 Capacity
 
4294967296
**4294967296**

- Ce qui équivaut à 4.00 Gigabytes.
---- ----

## Devices

Pour connaître la marque et le modèle de mon processeur je fais "**Get-CimInstance -ClassName Win32_Processor** | **Select-Object** -**ExcludeProperty** "**CIM***""

- *Ce qui m'affiche* : 

DeviceID Name                                     Caption                                MaxClockSpeed SocketDesignation Manufacturer

**CPU0**     **Intel(R) Core(TM) i7-8565U** CPU @ 1.80GHz Intel64 Family 6 Model 142 Stepping 11 1992          U3E1              GenuineIntel

- CPU0 est le nom de mon processeur, intel core i7-8565U est le modèle.

"**WMIC CPU Get DeviceID,NumberOfCores,NumberOfLogicalProcessors**" me donne : 
DeviceID  NumberOfCores  NumberOfLogicalProcessors
CPU0      4              8
- Son nombre de coeur, 4, et son nombre de processeurs, 8.
- Pour expliquer les nom de mon processeur, intel core est la marque du produit, i7 c'est le numéro qui va donner sa performance, 85 sont les deux premiers numéros et indiquent la génération, les autres chiffres sont les numéros de produit et ne sont pas utiles pour de la comparaison. Enfin la lettre U signifie que c'est un processeurs Intel Core mobiles bicœurs basse consommation.
---- -----

Pour trouver la marque et le modèle de ma carte graphique je fais " **Get-CimInstance win32_VideoController**".

- *Ce qui m'affiche* : 

Caption                      : **Intel(R) UHD Graphics 620** [...]
Caption                      : **NVIDIA GeForce GTX 1050 with Max-Q Design** [...]
---- ----

Pour identifier la marque et le modèle de mes disques durs je fais "**Get-PhysicalDisk**".

- *Ce qui m'affiche* : 

Number FriendlyName              SerialNumber MediaType CanPool OperationalStatus HealthStatus Usage            Size

0      **Micron_1100_MTFDDAV256TBN** 18301DB32079 SSD       False   OK                Healthy      Auto-Select 238.47 GB

- La marque de mon disque dur est Micron, et le modèle est le Micron_1100_MTFDDAV256TBN.


Pour la partition de mon disque dur je fais "**Get-Partition**".

- *Ce qui m'affiche* : 

 DiskPath : \\?\scsi#disk&ven_micron_1&prod_100_mtfddav256tb#4&111478e0&0&000200#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                                                           Size Type

1                            1048576                                                                        260 MB System
2                            273678336                                                                       16 MB Reserved
3                C           290455552                                                                   237.42 GB Basic
4                            255221301248                                                                 800 MB Recovery
- Dans la partition de mon disque dur j'ai au nuléro 1 le secteur de démarrage, le numéro "1048576" est l'adresse du début de la partition, "260 MB" est sa taille, "System" indique que cette partie est la partition du système. 
- Au numéro 2, qui commence à partir de "273678336", sa taille est de 16 MB, son type "Reserved" indique que cette partie est réservé au système, il contient le code du système.
- Au numéro 3, DriveLetter contient la lettre C, nous sommes donc situé sur mon disque dur C, c'est la mémoire que je peux utiliser sur mon ordinateur, elle est de 237,42 GB disponible, dedans il y a la partition principale. Son type est noté "Basic".
- Au numéro 4, qui commence à partir de "255221301248", nous avons 800 MB alloué pour la partition de récupération appellé "Recovery".
--- -----

## Users 

Pour afficher la liste des utilisateur de la machine je fais "**Get-CimInstance -ClassName Win32_OperatingSystem |**".

- *Ce qui m'affiche* : 

NumberOfLicensedUsers NumberOfUsers RegisteredUser
                                  2 mariec33127@gmail.com

- Soit un nombre total de 2 utilisateurs.

- Pour ce qui est de "NT-AUTHORITY\SYSTEM" il permet d'accéder au poste local. Il est décris comme un groupe mais agit comme un utilisateur. C'est le profil qui a tous les droits sur l'ordinateur. Il a toute fois une fonction différente de celle du compte admin. 

- Le SID n'a même pas besoin de définir un compte d'utilisateur ou un groupe. Il définit simplement un ensemble d'autorisations.
---------- --------

## Processus

Pour déterminer la liste des processus de la machine je fais "**Get-Service**".

- J'ai choisi d'expliquer "wscsvc             Centre de sécurité", sa fonction est de vérifier la présence d'un anti-virus activé, d'une protection pare-feu activée et des mises à jour automatiques activées.
- "Winmgmt Infrastructure de gestion Windows" est un système de gestion interne de Windows qui permet de contrôler et surveiller les ressources systèmes.
- "WcmsvcbGestionnaire des connexions Windows" prends des décisions automatiques de connexion/déconnexion en fonction des options de connectivité réseau actuellement disponibles à l’ordinateur, et active la gestion de la connectivité réseau en fonction des paramètres de la stratégie de groupe.
- "wuauserv windows update" est responsable du téléchargement et de l’installation automatique de logiciels créés par Microsoft sur votre ordinateur. C’est un composant essentiel pour garder son PC à jour avec les correctifs de sécurité essentiels.
- "mpssvc Pare-feu Windows Defender" filtre les connexions entrantes et sortantes afin de bloquer celles non autorisées.
--- ------

## Network

Pour afficher la liqte des cartes réseaux de ma machine je fais "**Get-NetAdapter | fl Name, InterfaceIndex**".

- *Ce qui m'affiche* : 

Name           : Connexion réseau Bluetooth
InterfaceIndex : 18

Name           : Wi-Fi
InterfaceIndex : 9

- Ma première carte réseau sert à  la connexion en bluetooth. Ma seconde sert à la connexion en Wifi.
----- ------

Pour lister tous les ports TCP et UDP en utilisation je fais "**netstat -a -b**".

- *Ce qui m'affiche* :

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            LAPTOP-B775J4NC:0      LISTENING
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            LAPTOP-B775J4NC:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:1717           LAPTOP-B775J4NC:0      LISTENING
 [ScreenPadOVPS.exe]
  TCP    0.0.0.0:5040           LAPTOP-B775J4NC:0      LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:49664          LAPTOP-B775J4NC:0      LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          LAPTOP-B775J4NC:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          LAPTOP-B775J4NC:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49667          LAPTOP-B775J4NC:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49668          LAPTOP-B775J4NC:0      LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49670          LAPTOP-B775J4NC:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.2.217:139        LAPTOP-B775J4NC:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.2.217:61313      40.67.251.132:https    ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    10.33.2.217:61396      wo-in-f188:5228        ESTABLISHED
 [chrome.exe]
  TCP    10.33.2.217:61878      ec2-52-193-206-70:https  ESTABLISHED
 [chrome.exe]
  TCP    10.33.2.217:61944      172.67.41.41:https     ESTABLISHED
 [chrome.exe]
  TCP    10.33.2.217:61953      162.159.134.234:https  ESTABLISHED
 [Discord.exe]
  TCP    10.33.2.217:61954      ec2-52-54-178-155:https  ESTABLISHED
 [chrome.exe]
  TCP    10.33.2.217:61955      ec2-52-54-178-155:https  ESTABLISHED
 [chrome.exe]
  TCP    10.33.2.217:61956      ec2-52-193-206-70:https  TIME_WAIT
  TCP    10.33.2.217:61957      a-0001:https           TIME_WAIT
  TCP    10.33.2.217:61961      13.107.18.11:https     TIME_WAIT
  TCP    10.33.2.217:61962      40.100.174.34:https    TIME_WAIT
  TCP    10.33.2.217:61963      bingforbusiness:https  TIME_WAIT
  TCP    10.33.2.217:61964      bingforbusiness:https  ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.217:61965      a-0001:https           ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.217:61966      40.90.23.153:https     ESTABLISHED
  wlidsvc
 [svchost.exe]
  TCP    10.33.2.217:61968      13.107.42.254:https    ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.217:61969      20.140.56.69:https     ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.217:61970      13.107.4.254:https     ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.217:61971      93.184.220.29:http     ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.217:61972      204.79.197.222:https   ESTABLISHED
 [SearchUI.exe]
  TCP    10.33.2.217:61973      20.44.232.74:https     ESTABLISHED
  CDPUserSvc_4d8e5fb
 [svchost.exe]
  TCP    127.0.0.1:6463         LAPTOP-B775J4NC:0      LISTENING
 [Discord.exe]
  TCP    127.0.0.1:54426        LAPTOP-B775J4NC:0      LISTENING
 [GiftBoxService.exe]
  TCP    [::]:135               LAPTOP-B775J4NC:0      LISTENING
  RpcSs
 [svchost.exe]
  TCP    [::]:445               LAPTOP-B775J4NC:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49664             LAPTOP-B775J4NC:0      LISTENING
 [lsass.exe]
  TCP    [::]:49665             LAPTOP-B775J4NC:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             LAPTOP-B775J4NC:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    [::]:49667             LAPTOP-B775J4NC:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    [::]:49668             LAPTOP-B775J4NC:0      LISTENING
 [spoolsv.exe]
  TCP    [::]:49670             LAPTOP-B775J4NC:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:49669            LAPTOP-B775J4NC:0      LISTENING
 [jhi_service.exe]
  UDP    0.0.0.0:5050           *:*
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5355           *:*
  Dnscache
 [svchost.exe]
  UDP    10.33.2.217:137        *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.2.217:138        *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.2.217:1900       *:*
  SSDPSRV
 [svchost.exe]
  UDP    10.33.2.217:63920      *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:1900         *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:49664        *:*
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:50183        *:*
  NlaSvc
 [svchost.exe]
  UDP    127.0.0.1:63921        *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::]:5353              *:*
 [chrome.exe]
  UDP    [::]:5353              *:*
 [chrome.exe]
  UDP    [::]:5353              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:5355              *:*
  Dnscache
 [svchost.exe]
  UDP    [::1]:1900             *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:63919            *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::b10c:1867:139a:4c76%9]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::b10c:1867:139a:4c76%9]:63918  *:*
  SSDPSRV
 [svchost.exe]
  - "svchost.exe" est un composant logiciel au service d'autres applications.
  - "ScreenPadOVPS.exe" fait parti de ASUSTek Computer Inc.
  - "lsass.exe" Local Security Authority Subsystem Service, c'est un processus dans les systèmes d’exploitation Microsoft Windows qui est responsable de l’application de la politique de sécurité sur le système.
  - "spoolsv.exe" est le principal service de file d'attente de l'imprimante. 
  - "chrome.exe" est un fichier exécutable qui exécute le navigateur Web Google Chrome.
  - "Discord.exe" est un fichier exécutable qui exécute l'application Discord.
  - "SearchUI.exe" active l’interface utilisateur de recherche de l’assistant de recherche Microsoft Cortana.
  - "GiftBoxService.exe" fait partie d’un produit appelé GiftBox dont le développeur est ASUSTeK Computer INC.
  - "jhi_service.exe" est un composant logiciel de Intel Management Engine Components par Intel Corporation.
  ----- -------
  
  ## II. Scripting 
  
  ------------
  
  ## III. Gestion des soft
  
L'intérêt de l'utilisation d'un gestionnaire de paquets par rapport au téléchargement en direct sur internet permet un téléchargement plus rapide. Premièrement on peut télécharger directement depuis le gestionnaire en faisant par exemple "choco install firefox" plutôt que de devoir chercher en ligne le bon fichier à télécharger, et deuxièment il permet de décompresser directement les paquets sur l'ordinateur. 
Au sujet de l'identité des gens impliqués dans un téléchargement, l'identité est protégée car il passe par un serveur tiers.
Quant à la sécurité globale impliquée lors d'un téléchargement, il sera bien sûr plus sécurisé s'il est effectué par le gestionnaire de paquets car il sera garantis sans virus contrairement aux téléchargements au lignes qui ne garantissent rien.

Pour ma part j'utilise Chocolatey car je suis sous windows 10. Pour lister tous les paquets je fais "choco list -l". 
Afin de trouver les informations relatives aux téléchargement effectués comme d'où viennent ils il faut faire "choco info firefox" firefox qui peut être remplacé par n'importe quel nom de logiciel déjà téléchargé.

## IV. Machine virtuelle

Je n'ai pas réussi à établir une connection ssh.
J'ai bien tééchargé Samba. Mais je n'ai pas réussi à le configurer avec mon utilisateu "test".
![](https://i.imgur.com/r9u05JZ.png)

![](https://i.imgur.com/iycgriU.png)

J'ai réussi à installer les paquets pour samba sur ma VM et créer un dossier de partage.
![](https://i.imgur.com/eqYJ3NY.png)

![](https://i.imgur.com/uuEsUwN.png)

Ma VM ne trouve pas mon dossier share, que j'ai créée dans mon profil d'utilisateur "test".
![](https://i.imgur.com/PpsKl7Z.png)

