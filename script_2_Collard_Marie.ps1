﻿#Script pour vérouiller ou éteindre son pc après un temps demandé, par Marie Collard 

$param = $args[0] #on récupère l'argument 0 pour savoir s'il s'agit de lock ou shutdown
$time = $args[1] #on récupère l'argument 1 pour savoir combien de temmps ce sera

if ($param -eq 'shutdown') { 
    if ($time){
        if ($time -is [int]) {
            Write-Host 'Vérouillage dans '$time ' secondes'
            Start-Sleep -Seconds $time #éteint le pc en foction du temps donné
            Stop-Computer #éteint le pc en foction du temps donné
        }
        else {
            Write-Host "un temps est nécessaire" 
        }
    }
    else {
        Write-Host 'you need to put a number : '
        Write-Host 'shutdown <time>'
    }
}
elseif ($param -eq 'lock') {
    if ($time) {
        if ($time -is [int]) {
            Write-Host ' Votre pc va être vérouillé dans '$time' secondes'
            Start-Sleep -Seconds $time
            rundll32.exe user32.dll,lockWorkStation #vérouille le pc en foction du temps donné
        }
        else {
            Write-Host 'non valide' 
        }
    }
    else {
        Write-Host 'Rentrer un chiffre'
        Write-Host 'lock <time>'
    }
}
else{
    Write-Host 'Rentrer un chiffre'
    Write-Host 'shutdown <time>''lock <time>'
}