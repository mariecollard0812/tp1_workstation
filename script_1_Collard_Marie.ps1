﻿#Script pour retourner le résumé des caractéristiques du système par Collard Marie INFO B1 A le 02/11/2020

$espace = "`n"

#titre
$infogenpc = $espace + 'INFORMATIONS GENERAL DE CE PC ' + $espace

#OS info 
$OSInfo = Get-WmiObject Win32_OperatingSystem | Select Caption, Version, CSName

#get pour la ram
$RAMinfo = Get-CIMInstance Win32_OperatingSystem | Select FreePhysicalMemory,TotalVisibleMemory,LastBootUpTime

#IP principale 
$IPAddress = Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object {$_.Ipaddress.length -gt 1} 
$ipout = 'Ip : '+$IPAddress.ipaddress[0]

#Nom de la machine
$name = 'Name : '+ $OSInfo.CSName

#OS name + version
$OSname = 'OS : '+ $OSInfo.Caption
$OSversion = 'Version : ' + $OSInfo.Version

$date = 'Boot depuis : ' + $RAMinfo.LastBootUpTime

#write tout
Write-Output $infogenpc,$ipout,$name,$OSname,$OSversion,$date

#ajouroupas
$uptimemaj = '10.0.19041' #déclaration de la version à jour
$up = 'Version à jour: true'
$down = 'Version à jour : false'
If ($uptimemaj -eq $OSInfo.Version) #comparaison avec ma version de l'os
{
    Write-Output $up
}
Else {
    Write-Output $down
}

Write-Output $inforam

#titre
$inforam = $espace + 'INFORMATIONS GENERALES SUR LA RAM ' + $espace

#ram usagée
$memory1 = $Mem - $memory2
$usedram = 'Used : ' + $memory1  + ' Gi'
$cs = get-wmiobject -class "Win32_ComputerSystem"
$Mem = [math]::Ceiling($cs.TotalPhysicalMemory / 1024 / 1024 / 1024)

#ram libre
$memory2 = $RAMinfo.FreePhysicalMemory * 1.0E-6
$freeram = 'Free : ' + $memory2 + ' Gi'
Write-Output $usedram,$freeram,$infousers


#titre
$infousers = $espace + 'INFORMATIONS SUR LES UTILISATEURS DE CE PC ' 

#listutilisateur
Get-LocalUser | select Name
Write-Output $infodiskdur

#espacedisque
$disk = Get-WmiObject Win32_LogicalDisk -ComputerName localhost -Filter "DeviceID='C:'" |
Select-Object Size,FreeSpace

$utilisédisk = $disk.size - $disk.FreeSpace #calcul de l'espace utilisé sur le disque dur physique principale
$convutilisédisk = 'Disk used : ' + [math]::Ceiling($utilisédisk / 1024 / 1024 / 1024) + ' G' #conversion en go

$convrestant = 'Memory free : ' + [math]::Ceiling($disk.FreeSpace / 1024 / 1024 / 1024) + ' G' 

#titre
$infodiskdur = $espace + 'INFORMATIONS SUR LE STOCKAGE DE CE PC ' + $espace

Write-Output $convutilisédisk, $convrestant,$infoping

#titre
$infoping = $espace + 'INFORMATIONS SUR LE PING ' + $espace
#$infospeedtest: $espace + 'speed test: ' +$espace

#ping
$rep_time = (Test-Connection -ComputerName "8.8.8.8" -Count 1  | measure-Object -Property ResponseTime -Average).average
$ping = 'Test du ping : ' +  $rep_time + 'ms'
Write-Output $ping

#speedtest
#$a=Get-Date; Invoke-WebRequest http://client.akamai.com/install/test-objects/10MB.bin|Out-Null; "$((10/((Get-Date)-$a).TotalSeconds)*8) Mbps"
